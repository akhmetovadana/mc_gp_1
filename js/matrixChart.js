
var MatrixChart = new function() {
  var container = '#matrixChart';
  var itemSize = 20,
  cellSize = itemSize - 1,
  margin = {top: 110, right: 0, bottom: 0, left: 50};
  var width = 420 - margin.right - margin.left,
  height = 350 - margin.top - margin.bottom;
  var martixChart;
  var totalMatrixElements = 0;

  var regions, indicators;
  var cellIndex = 0;
  this.init = function() {

    Promise.all([API.getRegions(), API.getIndicators(), API.getMatrix()])
    .then(function(results) {
      regions = results[0];
      indicators = results[1];
      regions[219].CRIME_ID_DESC = "ЮКО"
      regions[213].CRIME_ID_DESC = "ЗКО"
      regions[221].CRIME_ID_DESC = "СКО"
      regions[222].CRIME_ID_DESC = "ВКО"
      regions[223].CRIME_ID_DESC = "Астана"
      regions[224].CRIME_ID_DESC = "Алматы"

      var data = results[2];
      // sort data
      data.sort(function(a, b){
        var x = (+a.IND_ID) - (+b.IND_ID);
        return x == 0? (+a.VALUE_RATING) - (+b.VALUE_RATING) : x;
      });

      // console.table(data, ["IND_ID", "VALUE_RATING", "CRIME_REGION"]);
      var sortedRegions = [];
      for (var item of data) {
        if(item.IND_ID == 1) sortedRegions.push(item.CRIME_REGION);
      }
      var x_elements = sortedRegions,
      // d3.set(
      //   data.map(function( item, i ) { return item.CRIME_REGION; } )
      // ).values(),
      y_elements = d3.set(data.map(function( item ) { return item.IND_ID; } )).values();

      totalMatrixElements = x_elements.length * y_elements.length;

      var xScale = d3.scale.ordinal()
      .domain(x_elements)
      .rangeBands([0, x_elements.length * itemSize]);

      var xAxis = d3.svg.axis()
      .scale(xScale)
      .tickFormat(function (d) {
        return regions.filter(c => c.CRIME_REGION === d)[0].CRIME_ID_DESC.toUpperCase();
      })
      .orient("top");

      var yScale = d3.scale.ordinal()
      .domain(y_elements)
      .rangeBands([0, y_elements.length * itemSize]);

      var yAxis = d3.svg.axis()
      .scale(yScale)
      .tickFormat(function (d) {
        var desc = indicators.filter(i => i.IND_ID === d)[0].IND_DESC;
        $('.ind-list ul').append($("<li></li>").text('Инд'+ d + ': ' +desc));
        // for (let i=0;i<12;i++){
        //   if (short_case[i]["IND_ID"] == d){
        //       return short_case[i]["IND_DESC_SHORT"];
        //   }
        // }
        return 'Инд'+ d;
      })
      .orient("left");

      var tip = d3.tip()
      .attr('class', 'd3-tip')
      .direction('e')
      .offset([-10, 0])
      .html(function(d) {
        var rname = regions.filter(c => c.CRIME_REGION === d.CRIME_REGION)[0].CRIME_ID_DESC;
        for (i in short_case){
          if (d.IND_ID == short_case[i]["IND_ID"]){
            ind_name = short_case[i]["IND_DESC_SHORT"];
          }
        }
        return  "<strong>Индикатор:</strong> <span style='color:red'>" + ind_name + "</span><br/>"+
        "<strong>Регион:</strong> <span style='color:red'>" + rname + "</span><br/>"+
        "<strong>Количество:</strong> <span style='color:red'>" + (+d.IND_VALUE).toFixed(0) + "</span><br/>"+
        "<strong>Рейтинг:</strong> <span style='color:red'>" + (+d.VALUE_RATING) + "</span>";
      })

      martixChart = d3.select(container)
      .append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

      martixChart.call(tip);

      var cells = martixChart.selectAll('rect')
      .data(data)
      .enter().append('g').append('rect')
      .attr('width', cellSize)
      .attr('height', cellSize)
      .attr("rx", 4)
      .attr("ry", 4)
      .attr('x', function(d) { return xScale(d.CRIME_REGION); })
      .attr('y', function(d) { return yScale(d.IND_ID); })
      .attr("ind", function(d) { return d.IND_ID; })
      .attr("region", function(d) { return d.CRIME_REGION; })
      .attr('index', function (d, i) { return i;})
      .attr('fill', function(d) {
        if(d.VALUE_COLOR=='BLUE') return 'rgba(54,162,235,1)';
        if(d.VALUE_COLOR=='LITE_RED') return 'rgba(255,99,132,1)';
        if(d.VALUE_COLOR=='RED') return 'rgba(239,46,64,1)';
        else return Util.colorsMapping[d.VALUE_COLOR].color;
        })
      .attr('fill-opacity', 0.5)
      .attr('stroke', function(d) {
        if(d.VALUE_COLOR=='BLUE') return 'rgba(54,162,235,1)';
        if(d.VALUE_COLOR=='LITE_RED') return 'rgba(255,99,132,1)';
        if(d.VALUE_COLOR=='RED') return 'rgba(239,46,64,1)';
        else return Util.colorsMapping[d.VALUE_COLOR].color;
        })
      .attr('stroke-width', 2)
      .attr("class", "square")
      .on('mouseover', tip.show)
      .on('mouseout', tip.hide)
      .on('click', MatrixChart.changeMatrixCell);

      martixChart.append("g")
      .attr("class", "y axis")
      .call(yAxis)
      .selectAll('text')
      .attr('font-weight', 'normal');

      martixChart.append("g")
      .attr("class", "x axis")
      .call(xAxis)
      .selectAll('text')
      .attr('font-weight', 'normal')
      .attr('color', 'white')
      .style("text-anchor", "start")
      .attr("dx", ".5em")
      .attr("dy", ".5em")
      .attr("transform", function (d) {
        return "rotate(-65)";
      })


        //   svg.append("g")
        // .attr("class", "axisRed")
        // .call(d3.axisLeft(y));

        MatrixChart.changeMatrixCell();
      });
  };

  this.changeMatrixCell = function(d){
    Global.highcart_hovered = false;
    $('#kostanay').removeClass('invisible');
    if(martixChart !== undefined ){
      martixChart.selectAll('rect').attr("class", "square");
      if(d === undefined){
        var cell = martixChart.selectAll("rect").filter(function(d, i) { return i == cellIndex });
        Global.ind_id = cell.attr('ind');
        Global.region_id = cell.attr('region');
        cell.attr("class", "square current");
      }else{
        var cell = martixChart.selectAll("rect").filter(r => {
          return r.CRIME_REGION == d.CRIME_REGION && r.IND_ID == d.IND_ID });
        Global.ind_id = d.IND_ID;
        Global.region_id = d.CRIME_REGION;
        cell.attr("class", "square current");
        cellIndex = cell.attr('index');
        console.log('clicked');
        pauseMatrix();
      }
      // задаем жирность текущему региону в матрице по индексу
      var suitableCellIndex =  cellIndex -((Global.ind_id-1)*16);
      d3.select('.x.axis').selectAll('.tick').select('text').classed('currentRegion',false).filter(function(d, i) { return i == suitableCellIndex }).classed('currentRegion',true)
      cellIndex++;
      Global.matrixChanged();
    }
    if(cellIndex > totalMatrixElements-1) cellIndex = 0
  }
}

var matrixTimer;
function pauseMatrix() {
  $('.play-btn').addClass('paused');
  console.log('pause');
  clearInterval(matrixTimer);
}
function startMatrix() {
  console.log('play');
  matrixTimer = setInterval('MatrixChart.changeMatrixCell()', 60000);
}

$(function(){
  MatrixChart.init();
  startMatrix();
})
