var IND_ID = -1;
var CRIME_REGION;
var setIndId = function(ind_id){
    IND_ID = ind_id;
};

var getIndId = function(){
    return IND_ID;
};

var setCrimeRegion = function(region){
    CRIME_REGION = region;
};

var getCrimeRegion = function(){
    return CRIME_REGION;
};
Global.highcart_hovered = false;
// (function (H) {
//     H.wrap(H.Tooltip.prototype, 'hide', function (defaultCallback) {
// });
// }(Highcharts));
var chart = Highcharts.chart('pieChart', {
    chart: {
        type: 'pie',
        height: 200,
        width: 200,
        backgroundColor:'rgba(255, 255, 255, 0.0)'
    },
    title: {
        text: ''
    },
    plotOptions: {
        series: {
            dataLabels: {
                enabled: false
            },
            point: {
                events: {
                    click: function() {
                        Global.region_id = this.id;
                        var reg = {
                            CRIME_REGION: Global.region_id,
                            IND_ID: Global.ind_id
                        };
                        MatrixChart.changeMatrixCell(reg);
                        Global.matrixChanged();
                    },
                    mouseOver: function () {
                        Global.highcart_hovered = true;
                    }
                }
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{point.name}</span><br>',
        pointFormat: '<span>{point.name}</span>: <b>{point.y:.0f}%</b><br/>',
        hideDelay: 10000000
    },
    credits: {
        enabled: false
    },
    series: [{
        name: '',
        colorByPoint: true
    }]
});

var getAPImtc = function() {
    Promise.all([API.getRegionTitles(), API.getMtcYear(Global.ind_id)]).then(function (res) {
        var data = res[1];
        data.sort(function compare(a,b) {
            if (parseInt(b.IND_VALUE_P*100) < parseInt(a.IND_VALUE_P*100))
                return -1;
            if (parseInt(b.IND_VALUE_P*100) > parseInt(a.IND_VALUE_P*100))
                return 1;
            return 0;
        });
        data = data.filter(function (a) {
            return parseInt(a.CRIME_REGION) >= 1900 ;
        });
        var regions = res[0];
        function quotesDonutData(){

            return data.map(function(d) {

                for (var j = 0; j < regions.length; j++) {
                    if(regions[j].CRIME_REGION == d.CRIME_REGION) {
                        // var rname = regions.filter(c => c.CRIME_REGION === salesData[j].oblast)[0].CRIME_ID_DESC;
                        return {name:regions[j].CRIME_REGION_DESC, y: parseInt(d.IND_VALUE_P*100), id:d.CRIME_REGION};
                    }
                    if (d.CRIME_REGION == Global.region_id) {
                        $('#ind_value_p').text(parseInt(d.IND_VALUE_P*100)+"%");
                    }
                }
            });
        }
        chart.series[0].setData(quotesDonutData(), true);
        chart.get(Global.region_id).setState('hover');
        // chart.tooltip.refresh(chart.get(Global.region_id));
    })
};
setInterval(function(){
    if (typeof chart.get(Global.region_id) !== "undefined") {
        // console.log(Global.highcart_hovered);
        // if (Global.highcart_hovered) {
        //     chart.get(Global.region_id).setState('hover');
        //     chart.tooltip.refresh(chart.get(Global.region_id));
        // }
        // else {
        //     chart.get(Global.region_id).setState('default');
        //     chart.tooltip.hide();
        // }
    }
}, 1000);