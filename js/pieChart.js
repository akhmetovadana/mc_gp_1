 var IND_ID = -1;
 var CRIME_REGION
 var setIndId = function(ind_id){
 	IND_ID = ind_id;
 }

 var getIndId = function(){
 	return IND_ID;
 }

 var setCrimeRegion = function(region){
 	CRIME_REGION = region;
 }

 var getCrimeRegion = function(){
 	return CRIME_REGION;
 }


 var getAPImtc = function(){
 	API.getMtcYear(Global.ind_id)
 	.then(function(res){
 		var data = res;
 		var regions = [];
 		var salesData=[
 		{oblast:"1911", color: "#808080"},
 		{oblast:"1915", color: "#A9A9A9"},
 		{oblast:"1919", color: "#C0C0C0"},
 		{oblast:"1923", color: "#D3D3D3"},
 		{oblast:"1927", color: "#DCDCDC"},
 		{oblast:"1931", color: "#6495ED"},
 		{oblast:"1935", color: "#1E90FF"},
 		{oblast:"1939", color: "#00BFFF"},
 		{oblast:"1943", color: "#4682B4"},
 		{oblast:"1947", color: "#4169E1"},
 		{oblast:"1951", color: "#6A5ACD"},
 		{oblast:"1955", color: "#9370DB"},
 		{oblast:"1959", color: "#BA55D3"},
 		{oblast:"1963", color: "#9932CC"},
 		{oblast:"1971", color: "#8A2BE2"},
 		{oblast:"1975", color: "#483D8B"}
 		];

 		$('#pieChart').empty()
 		var svg = d3.select("#pieChart").append("svg").attr("width",180).attr("height",120);

 		svg.append("g").attr("id","quotesDonut");

 		Donut3D.draw("quotesDonut", quotesDonutData(),100, 50,80, 40, 30, 0);


 		function salesDonutData(){

 			return salesData.map(function(d){

 				for (var j = 0; j < data.length; j++) {
 					if(d.oblast == data[j].CRIME_REGION){
 						// if(d.oblast === Global.region_id){
 						// 	return {oblast: d.oblast, value: data[j].IND_VALUE_PERCENT, color: "black"};
 						// 	break;
 						// }
 						return {oblast:d.oblast, value: data[j].IND_VALUE_PERCENT, color:d.color};
 						break;
 					}
 				}
 			});
 		};

 		function quotesDonutData(id){

 			return salesData.map(function(d){

 				for (var j = 0; j < data.length; j++) {
 					if(d.oblast == data[j].CRIME_REGION){
 						// if(d.oblast === Global.region_id){
 						// 	return {oblast: d.oblast, value: data[j].IND_VALUE, color: "black"};
 						// 	break;
 						// }
 						return {oblast:d.oblast, value: data[j].IND_VALUE, color:d.color};
 						break;
 					}
 				}
 			});
 		}


 	})};
