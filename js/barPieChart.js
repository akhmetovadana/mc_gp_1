function regionChart(fData, regionInd){
   var barColor = 'steelblue';
   var regionBlock = "region"+regionInd;
   var $div = $("<div>", {id: regionBlock, class: 'region-block'});
   var header = $("<h3>", {class: 'text-center'}).text(fData.regionName);
   $div.append(header);
   $("#regionsContainer").append($div);

   function getColor(c){
     var colors=[
       {color: "#CD5C5C"},
       {color: "#DC143C"},
       {color: "#DB7093"},
       {color: "#FF7F50"},
       {color: "#FFD700"},
       {color: "#EE82EE"},
       {color: "#BA55D3"},
       {color: "#8A2BE2"},
       {color: "#32CD32"},
       {color: "#008000"},
       {color: "#008080"},
       {color: "#4682B4"},
       {color: "#1E90FF"},
       {color: "#7B68EE"},
       {color: "#0000FF"},
       {color: "#4B0082"},
       {color: "rgb(255, 255, 0)"},
       {color: "rgb(148, 156, 0)"},
     ];
     return colors[c].color;
   }

   // bar chart
   function histoGram(fD){

       var hG={}, hGDim = {t: 20, r: 20, b: 50, l: 40};
       hGDim.w = 500 - hGDim.l - hGDim.r,
       hGDim.h = 300 - hGDim.t - hGDim.b;

       //create svg for histogram.
       var hGsvg = d3.select('#'+regionBlock).append("svg")
       .attr("width", hGDim.w + hGDim.l + hGDim.r)
       .attr("height", hGDim.h + hGDim.t + hGDim.b).append("g")
       .attr("transform", "translate(" + hGDim.l + "," + hGDim.t + ")");

       var color = d3.scale.ordinal()
         .range(["steelblue", "#f1f1f1"]);

       // create function for x-axis mapping.
       var x = d3.scale.ordinal().rangeRoundBands([0, hGDim.w], 0.1)
       .domain(fD.map(function(d) { return d[0]; }));

       var x0 = d3.scale.ordinal()
         .rangeRoundBands([0, hGDim.w], .1);

       var x1 = d3.scale.ordinal();

       var xAxis = d3.svg.axis()
         .scale(x0)
         .tickSize(0)
         .orient("bottom");

       var monthNames = fD.map(function(d) { return d.monthName; });
       var years = fD[0].values.map(function(d) { return d.year; });

       x0.domain(monthNames);
       x1.domain(years).rangeRoundBands([0, x0.rangeBand()]);

       // Add x-axis to the histogram svg.
       hGsvg.append("g").attr("class", "x axis")
         .attr("transform", "translate(0," + hGDim.h + ")")
         .call(xAxis)
         .selectAll("text")
         .attr("x", 3)
         .attr("transform", "rotate(45)")
         .style("text-anchor", "start");

       // Create function for y-axis map.
       var y = d3.scale.linear().range([hGDim.h, 0])
          .domain([0, d3.max(fD, function(month) { return d3.max(month.values, function(d) { return d.value; }); })]);

       var yAxis = d3.svg.axis()
         .scale(y)
         .orient("left");

      hGsvg.append("g")
        .attr("class", "y axis")
        .call(yAxis)
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
        .style('font-weight','bold');

      hGsvg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + hGDim.h + ")")
        .call(xAxis)
        .selectAll("text")
        .attr("x", 3)
        .attr("transform", "rotate(45)")
        .style("text-anchor", "start");

      var stip = d3.tip()
        .attr('class', 'd3-tip')
        .offset([-10, 0])
        .html(function(d) {
          console.log(d);
          return  "<strong>"+d.value+"</strong>";
        });
      hGsvg.call(stip);

      var bars = hGsvg.selectAll(".bar")
        .data(fD)
        .enter().append("g")
        .attr("class", "bar")
        .attr("transform",function(d) { return "translate(" + x0(d.monthName) + ",0)"; });

      bars.selectAll("rect")
        .data(function(d) { return d.values; })
        .enter().append("rect")
        .attr("width", x1.rangeBand())
        .attr("x", function(d) { return x1(d.year); })
        .style("fill", function(d) { return color(d.year) })
        .attr("y", function(d) { return y(d.value); })
        .attr("height", function(d) { return hGDim.h - y(d.value); })
        .on("mouseover", mouseover)
        .on("mouseout", mouseout);

      var legend = hGsvg.selectAll(".legend")
        .data(["2016", "2017"])
        .enter().append("g")
        .attr("class", "legend")
        .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });

      legend.append("rect")
        .attr("x", hGDim.w - 18)
        .attr("width", 18)
        .attr("height", 18)
        .style("fill", color);

      legend.append("text")
        .attr("x", hGDim.w - 24)
        .attr("y", 9)
        .attr("dy", ".35em")
        .style("text-anchor", "end")
        .text(function(d) { return d; });

      function mouseover(d){  // utility function to be called on mouseover.
        stip.show(d);
        d3.select(this).style("fill", d3.rgb(color(d.year)).darker(2));
        pC.update(d.districts);
      }

      function mouseout(d){    // utility function to be called on mouseout.
        stip.hide();
        d3.select(this).style("fill", color(d.year));
        pC.update(fData.districts);
      }

      // create function to update the bars. This will be used by pie-chart.
      hG.update = function(newArray){
        y.domain([0, d3.max(newArray, function(month) { return d3.max(month.values, function(d) {  return d.value; }); })]);
        var new_bars = hGsvg.selectAll(".bar").data(newArray);
        hGsvg.select(".y.axis").transition().duration(300).call(yAxis)
        new_bars.selectAll("rect")
            .data(function(d) { return d.values; })
            .transition()
            .delay(function (d) {return Math.random()*1000;})
            .duration(500)
            .attr("y", function(d) { return y(d.value); })
            .attr("height", function(d) { return hGDim.h - y(d.value); });

      }
      return hG;
   }
   // function to handle §.
   function pieChart(pD){
     var pC ={}, pieDim ={w:200, h: 200};
     pieDim.r = Math.min(pieDim.w, pieDim.h) / 2;

     // create svg for pie chart.
     var piesvg = d3.select('#'+regionBlock).append("svg")
     .attr("width", pieDim.w).attr("height", pieDim.h).append("g")
     .attr("transform", "translate("+pieDim.w/2+","+pieDim.h/2+")");
     var total = d3.sum(pD, function(d) {
        return (+d.value);
      });
     // create function to draw the arcs of the pie slices.
     var arc = d3.svg.arc().outerRadius(pieDim.r - 10).innerRadius(0);
     // create a function to compute the pie slice angles.
     var pie = d3.layout.pie().sort(null).value(function(d) { return d.value; });
     var stip = d3.tip()
       .attr('class', 'd3-tip')
       .offset([-10, 0])
       .html(function(d) {
         var rname = d.data.name;
         var rpercent = ((d.data.value * 100)/ total).toFixed(2);
         return  "<strong>"+rname+":</strong> <span style='color:red'>"+rpercent+"%</span>";
       });
     piesvg.call(stip);
     // Draw the pie slices.
     piesvg.selectAll("path").data(pie(pD)).enter().append("path").attr("d", arc)
       .each(function(d) { this._current = d; })
       .style("fill", function(d) {
         var code = (+d.data.code) % 10 + (+d.data.code[d.data.code.length - 2]);
         return getColor(code);
       })
       .on('mouseover', mouseover)
       .on('mouseout', mouseout);

     // create function to update pie-chart. This will be used by histogram.
     pC.update = function(nD){
       piesvg.selectAll("path").data(pie(nD)).transition().duration(500).attrTween("d", arcTween);
     }
     // Utility function to be called on mouseover a pie slice.
     function mouseover(dt){
       stip.show(dt);
       var newArray = $.extend(true, [], fData.monthes);
       newArray.map(dis => {
         dis.values[0].value = 0;
         dis.values[1].value = 0;
         for(let vals of dis.values) {
           vals.value = 0;
           vals.districts = vals.districts.map(d => {
             if(d.code === dt.data.code) vals.value = d.value;
           });
         }
       });
       hG.update(newArray);
     }
     //Utility function to be called on mouseout a pie slice.
     function mouseout(d){
       stip.hide();
       hG.update(fData.monthes);
     }

     // Animating the pie-slice requiring a custom function which specifies
     // how the intermediate paths should be drawn.
     function arcTween(a) {
       var i = d3.interpolate(this._current, a);
       this._current = i(0);
       return function(t) { return arc(i(t));    };
     }
     return pC;
   }

    function legend(lD){
      lD = lD.filter(d => {if((+d.value) > 0) return d});
      var leg = {};
      // create table for legend.
      var legend = d3.select('#'+regionBlock).append("table").attr('class','table region-table');
      // create one row per segment.
      var tr = legend.append("tbody").selectAll("tr").data(lD).enter().append("tr");
      // create the first column for each segment.
      tr.append("td").append("svg").attr("width", '10').attr("height", '10').append("rect")
        .attr("width", '10').attr("height", '10')
        .attr("fill",function(d){
          var code = (+d.code) % 10 + (+d.code[d.code.length - 2]);
          return getColor(code);
        });
      // create the second column for each segment.
      tr.append("td").text(function(d){return d.name;});
      // Utility function to be used to update the legend.
      leg.update = function(nD){
        // update the data attached to the row elements.
        var l = legend.select("tbody").selectAll("tr").data(nD);
        // update the frequencies.
        l.select(".legendFreq").text(function(d){ return d3.format(",")(d.code);});
        // update the percentage column.
        l.select(".legendPerc").text(function(d){ return getLegend(d,nD);});
      }
      return leg;
    }

    var hG = histoGram(fData.monthes),
        pC = pieChart(fData.districts),
        leg = legend(fData.districts);
}

var current_ind ;
var regions = {}, regionsArray = [], districtsData = [];

var init = function(){
  API.getIndicators()
    .then(function(res){
       var data = res;
       data.map(ind => {
         $('.indicator-select')
         .append($("<option></option>")
                    .attr("value", ind.IND_ID)
                    .text(ind.IND_DESC));
       })
       $(".indicator-select").val("1");
       getChartData(1);
    });
}
$('.indicator-select').on('change', function() {
  $('#regionsContainer').empty();
  var val = $(this).val();
  getChartData(val);
})

function objToArray(obj) {
 var arr = [];
 for (var key in obj) {
   if (!obj.hasOwnProperty(key))
     continue;
   arr.push(obj[key]);
 }
 return arr;
}

function getChartData(ind_value){

  Promise.all([API.getYearExtendedData(ind_value), API.getRegions()])
   .then(function(results) {
     var data = results[0];
     var regionsData = results[1];

     data.forEach(function(x) {
       var rname = regionsData.filter(r => (+r.CRIME_REGION) === (+x.CRIME_REGION))[0].CRIME_ID_DESC;
       if (!regions[x.CRIME_REGION]) {
         regions[x.CRIME_REGION] = {
           regionCode: x.CRIME_REGION,
           regionName: rname,
           districts: [],
           monthes: [],
         };
       }

       var region = regions[x.CRIME_REGION];
       var district = region.districts.find(e => {
         return (e.code === x.CRIME_DIST)?e:null;
       });
       if(district === undefined) {
         var dname = regionsData.filter(r => (+r.CRIME_REGION) === (+x.CRIME_DIST))[0];
         if(dname === undefined) dname = 'не определен';
         district = {
           name: dname.CRIME_ID_DESC,
           code: x.CRIME_DIST,
           year: x.CRIME_YEAR,
           value: (+x.IND_VALUE_PERCENT),
         };
         if(+x.CRIME_YEAR === 2017){
           region.districts.push(district);
         }
       }else{
         if(+x.CRIME_YEAR === 2017){
           district.value += (+x.IND_VALUE_PERCENT);
         }
       }

       var districtObj = {
         name: 'district-'+x.CRIME_DIST,
         code: x.CRIME_DIST,
         year: x.CRIME_YEAR,
         value: (+x.IND_VALUE_PERCENT)
       };
       var month = region.monthes.find(e => {
         return (e.month === x.CRIME_MONTH)?e:null;
       });
       if(month === undefined) {
         month = {
           month: x.CRIME_MONTH,
           monthName: Util.Monthes[+x.CRIME_MONTH - 1],
           values: [
             {
               year: "2016",
               value: 0,
               districts: []
             },
             {
               year: "2017",
               value: 0,
               districts: []
             }
           ]
         };
         if(+x.CRIME_YEAR === 2016) {
           month.values[0].value = (+x.IND_VALUE);
           month.values[0].districts.push(districtObj);
         }else{
           month.values[1].value = (+x.IND_VALUE);
           month.values[1].districts.push(districtObj);
         }
         region.monthes.push(month);
       }else{
         var year = month.values.find(e => {
           return (e.year === x.CRIME_YEAR)?e:null;
         });
         year.value += (+x.IND_VALUE);
         year.districts.push(districtObj);
       }

       region.monthes.sort(function (a, b) {
         return (+a.month) - (+b.month);
       });
     });

     regionsArray = objToArray(regions);
     regionsArray.map((r, i) => {
       regionChart(r, i);
     });

  });

}

init();
