function StackedChartYear() {
  var container = '#stackedChartYear';
  $(container).empty();
  $('.stacked-tip').remove();
  var margin = {top: 20, right: 20, bottom: 50, left: 40},
  width = 350 - margin.left - margin.right,
  height = 180 - margin.top - margin.bottom;

  var x0 = d3.scale.ordinal()
  .rangeRoundBands([0, width], 0.2);

  var x1 = d3.scale.ordinal();

  var y = d3.scale.linear()
  .range([height, 0]);

  var xAxis = d3.svg.axis()
  .scale(x0)
  .orient("bottom");

  var yAxis = d3.svg.axis()
  .scale(y)
  .orient("left")
  .tickFormat(d3.format(".2s"));

  // var color = d3.scale.ordinal()
  //     .range(["#CD5C5C", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);
  var svg = d3.select(container).append("svg")
  .attr("width", width + margin.left + margin.right)
  .attr("height", height + margin.top + margin.bottom)
  .append("g")
  .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  var yBegin;

  var innerColumns = {
    "column1" : ["2016-1","2016-2","2016-3", "2016-4","2016-5","2016-6","2016-7", "2016-8", "2016-9", "2016-10", "2016-11", "2016-12"],
    "column2" : ["2017-1","2017-2","2017-3", "2017-4","2017-5","2017-6","2017-7", "2017-8", "2017-9", "2017-10", "2017-11", "2017-12"],
  }
  var col1 = new Set();
  var col2 = new Set();
  // this.init = function(ind) {
    // clear svg


    Promise.all([API.getChart1Data(Global.ind_id), API.getRegions()])
    .then(function(results) {
      var result = results[0];
      var regions = results[1];

      result.map(d => {
        if(d.CRIME_YEAR === "2016" && d.CRIME_REGION !== "1900") col1.add(d.CRIME_YEAR+"-"+d.CRIME_REGION);
        if(d.CRIME_YEAR === "2017" && d.CRIME_REGION !== "1900") col2.add(d.CRIME_YEAR+"-"+d.CRIME_REGION);
      });

      var chartData = {};
      result.forEach(function(x) {
        if(x.CRIME_REGION !== "1900"){
          var month = x.CRIME_MONTH;
          var monthName = Util.Monthes[+x.CRIME_MONTH - 1];
          if (!chartData[month]) {
            chartData[month] = {
              "CRIME_MONTH": monthName,
            };
          }

          var chartData = {};
          result.forEach(function(x) {
            if(x.CRIME_REGION !== "1900"){
              var month = x.CRIME_MONTH;
              var monthName = Util.Monthes[+x.CRIME_MONTH - 1];
              if (!chartData[month]) {
                chartData[month] = {
                  "CRIME_MONTH": monthName,
                };
              }

              if(x.CRIME_YEAR === "2016" && x.CRIME_MONTH === month) {
                var name = x.CRIME_YEAR + "-"+x.CRIME_REGION;
                col1.add(name);
                chartData[month][name] = x.IND_VALUE;
              }
              if(x.CRIME_YEAR === "2017" && x.CRIME_MONTH === month) {
                var name = x.CRIME_YEAR + "-"+x.CRIME_REGION;
                col2.add(name);
                chartData[month][name] = x.IND_VALUE;
              }
            }
          });
          innerColumns.column1 = [...col1];
          innerColumns.column2 = [...col2];

          var data = [];
          for(var index in chartData) {
            data.push(chartData[index]);
          }
          console.log('stacked chart data loaded');
        // if (error) throw error;
        var columnHeaders = d3.keys(data[0]).filter(function(key) { return key !== "CRIME_MONTH" && key !== "region"; });
        // color.domain(d3.keys(data[0]).filter(function(key) { return key !== "CRIME_MONTH"; }));
        data.forEach(function(d) {
          var yColumn = new Array();
          d.columnDetails = columnHeaders.map(function(name) {
            for (ic in innerColumns) {
              if($.inArray(name, innerColumns[ic]) >= 0){
                // console.log(d[name]);
                if (!yColumn[ic]){
                  yColumn[ic] = 0;
                }
                yBegin = yColumn[ic];
                yColumn[ic] += +d[name];
                return {
                  name: name,
                  column: ic,
                  yBegin: yBegin,
                  yEnd: +d[name] + yBegin,
                };
              }
            }
          });
          d.total = d3.max(d.columnDetails, function(d) {
            return d.yEnd;
          });
        });
        x0.domain(data.map(function(d) { return d.CRIME_MONTH; }));
        x1.domain(d3.keys(innerColumns)).rangeRoundBands([0, x0.rangeBand()]);

        y.domain([0, d3.max(data, function(d) {
          return d.total;
        })]);

        var stip = d3.tip()
        .attr('class', 'stacked-tip')
        .direction('n')
        .offset([-10, 0])
        .html(function(d) {
          var v = d.yEnd - d.yBegin;
          var rid = d.name.substr(5,4);
          var rname = regions.filter(r => (+r.CRIME_REGION) === (+rid))[0].CRIME_ID_DESC;

          return  "<strong>Регион:</strong> <span style='color:red'>"+rname+"</span><br/>"+
          "<strong>Значение:</strong> <span style='color:red'>"+v +"</span>";
        });

        svg.call(stip);

        svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis)
        .selectAll("text")
        .attr("x", 3)
        .attr("transform", "rotate(45)")
        .style("text-anchor", "start");

        svg.append("g")
        .attr("class", "y axis")
        .call(yAxis)
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".7em")
        .style("text-anchor", "end")
        .text("");
      // рисуем горизонтальные линии
      d3.selectAll("g.y.axis g.tick")
      .append("line")
      .classed("grid-line", true)
      .attr("x1", 0)
      .attr("y1", 0)
      .attr("x2",w)
      .attr("y2", 0);

      var project_stackedbar = svg.selectAll(".project_stackedbar")
      .data(data)
      .enter().append("g")
      .attr("class", "g")
      .attr("transform", function(d) { return "translate(" + x0(d.CRIME_MONTH) + ",0)"; });

      project_stackedbar.selectAll("rect")
      .data(function(d) { return d.columnDetails; })
      .enter().append("rect")
      .attr("width", x1.rangeBand())
      .attr("x", function(d) {
        return x1(d.column);
      })
      .attr("y", function(d) {
        if(!isNaN(d.yEnd))
          return y(d.yEnd);
      })
      .attr("height", function(d) {
        if(!isNaN(d.yEnd))
          return y(d.yBegin) - y(d.yEnd);
      })
      .style("fill", function(d) {
        var r = d.name.substr(5,4);
        return Util.regionColor[r].color;
      });
      .attr("class", "y axis")
      .call(yAxis)
      .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".7em")
      .style("text-anchor", "end")
      .text("");

      var project_stackedbar = svg.selectAll(".project_stackedbar")
      .data(data)
      .enter().append("g")
      .attr("class", "g ")
      .attr("transform", function(d) { return "translate(" + x0(d.CRIME_MONTH) + ",0)"; });

      project_stackedbar.selectAll("rect")
      .data(function(d) { return d.columnDetails; })
      .enter().append("rect")
      .attr("width", x1.rangeBand())
      .attr("x", function(d) {
        return x1(d.column);
      })
      .attr("y", function(d) {
        if(!isNaN(d.yEnd))
          return y(d.yEnd);
      })
      .attr("height", function(d) {
        if(!isNaN(d.yEnd))
          return y(d.yBegin) - y(d.yEnd);
      })
      .style("fill", function(d) {
        var r = d.name.substr(5,4);
        return Util.regionColor[r].color;
      })
      .on('mouseover', stip.show)
      .on('mouseout', stip.hide);
    });
  // }
}
