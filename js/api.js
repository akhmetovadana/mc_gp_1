function ajaxInner(url, data) {
	return new Promise(function(resolve, reject) {
		$.get(url, data || {})
		.done(function(data) {
			if (!data.success) {
				console.error("Error: " + data.message);
				reject("Not success! Error: " + data.message);
			} else {
				resolve(data.list || {});
			}
		})
		.fail(function(err) {
			reject(err);
		});
	});
}

var API = new function() {
	this.baseUrl = "http://qamqor.gov.kz/ias2/";
	this.getRegions = function() {
		return ajaxInner(this.baseUrl + "ias1_crime_region").then(function(list) {
			return list;
		});
	};
	this.getMTCRegions = function() {
		return ajaxInner(this.baseUrl + "ias_mtc_crime_region").then(function(list) {
			return list;
		});
	};
	this.getCrimes = function() {
		return ajaxInner(this.baseUrl + "IAS2_CRIME_ID").then(function(list) {
			return list;
		});
	}
	this.getIndicators = function() {
		return ajaxInner(this.baseUrl + "ias_mtc_ind").then(function(list) {
			return list;
		});
	};
	this.getMatrix = function() {
		return ajaxInner(this.baseUrl + "ias_mtc_matrix").then(function(list) {
			return list;
		});
	};
	this.getMtcYear = function(ind_id){
		return ajaxInner(this.baseUrl + "ias_mtc_str", {
			IND_ID: ind_id
		}).then(function(list){
			return list;
		});
	},

	this.getRegionsData = function(ind_id){
		return ajaxInner(this.baseUrl + "ias_mtc_str_mean", {
			IND_ID: ind_id
		}).then(function(list){
			return list;
		});
	},

	this.getTitleRegion = function(){
		return ajaxInner("http://service.pravstat.kz/ias2/ias1_crime_region").then(function(list) {
			return list;
		});
	}

    this.getRegionTitles = function(){
        return ajaxInner(this.baseUrl + 'ias_mtc_crime_region').then(function(list) {
            return list;
        });
    };


	this.getPopulationData = function(){
		return ajaxInner(this.baseUrl + "ias_mtc_population").then(function(list) {
			return list;
		});
	}

	this.getChart1Data = function(ind_id){
		return ajaxInner(this.baseUrl + "ias_mtc_year", {
			IND_ID: ind_id
		}).then(function(list){
			return list;
		});
	},
	this.getChart2Data = function(ind_id,crime_region){
		return ajaxInner(this.baseUrl + "ias_mtc_month", {
			IND_ID: ind_id,
			CRIME_REGION:crime_region
		}).then(function(list){
			return list;
		});
	},
	this.getChart3Data = function(ind_id,crime_region){
		return ajaxInner(this.baseUrl + "ias_mtc_day", {
			IND_ID: ind_id,
			CRIME_REGION:crime_region
		}).then(function(list){
			return list;
		});
	},
	this.getChart3DataRaion = function(ind_id,crime_region){
		console.log(ind_id,crime_region)
		return ajaxInner(this.baseUrl + "ias_mtc_day_dist", {
			IND_ID: ind_id,
			CRIME_REGION:crime_region
		}).then(function(list){
			return list;
		});
	}
	this.getYearExtendedData = function(ind_id){
		return ajaxInner(this.baseUrl + "ias_mtc_str_crime", {
			IND_ID: ind_id
		}).then(function(list){
			return list;
		});
	},
	this.getPeriods = function() {
		return ajaxInner(this.baseUrl + "ias1_crime_time", {
		}).then(function(list){
			return list;
		});
	}
}

var d = new Date();
var current_month = d.getMonth()-1;
function setPrisost(ind_id,region_id){
	var settings = {
		"async": true,
		"crossDomain": true,
		"url": "http://qamqor.gov.kz/ias2/ias_mtc_year?IND_ID="+ind_id,
		"method": "GET"
	}
	$.ajax(settings).done(function (response) {
		var resp = response["list"];
		// for (i in resp){
		// 	if(resp[i]["CRIME_REGION"]=="19"){
		// 		if(resp[i]["CRIME_YEAR"]=="2016"){
		// 			if(resp[i]["PRIROST"]!="0"){
		// 				var prirost = Math.round(Number(resp[i]["PRIROST"])*100);
		// 				if (prirost<0){
		// 					$("#prirost").text(prirost+"% ").css('color', 'blue');
		// 				}else{
		// 					$("#prirost").text("+"+prirost+"% ").css('color', 'red');
		// 				}
		// 			}
		// 		}
		// 	}
		// }
		var settings = {
			"async": true,
			"crossDomain": true,
			"url": "http://qamqor.gov.kz/ias2/ias_mtc_year?IND_ID="+ind_id,
			"method": "GET"
		}

		$.ajax(settings).done(function (response) {
			var resp = response["list"];
			var ind_val_16 = "";
			var ind_val_17 = "";
			for (i in resp){
				if(resp[i]["CRIME_REGION"]=="19"){
					if(resp[i]["CRIME_MONTH"]==current_month){
						if(resp[i]["CRIME_YEAR"]=="2016"){
							ind_val_16 = resp[i]["IND_VALUE"];
						}
						if(resp[i]["CRIME_YEAR"]=="2017"){
							ind_val_17 = resp[i]["IND_VALUE"];
						}
					}
				}
			}
			$("#detail_prirost").text(ind_val_16+" в 2016 г./"+ind_val_17+" в 2017 г.");
		});
	});
}
function setOtklonenie(ind_id,region_id){
	var settings = {
		"async": true,
		"crossDomain": true,
		"url": "http://qamqor.gov.kz/ias2/ias_mtc_month?CRIME_REGION="+region_id+"&IND_ID="+ind_id,
		"method": "GET"
	}

	$.ajax(settings).done(function (response) {
		var resp = response["list"];
		var otkl = "";
		for (i in resp){
			if (resp[i]["IND_TIME"].split()[0].split("-")[1]==current_month){
				otkl = Math.round(Number(resp[i]["IND_DEV"])*100);
				if (Number(resp[i]["IND_DEV"])<0){
					$("#otklonenie").text(otkl+"%").css('color', 'blue');
					$("#detail_otklonenie").text(" Отклонение от среднего по РК");
				}else{
					$("#otklonenie").text("+"+otkl+"%").css('color', 'red');
					$("#detail_otklonenie").text(" Отклонение от среднего по РК");
				}
			}
		}
	});
}
