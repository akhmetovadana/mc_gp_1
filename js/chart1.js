function drawChart1(ind_id,region_id) {
  API.getChart1Data(ind_id, region_id).then(function(data) {
   var $container = $("#chart-1").empty();
   var $canvas = $("<canvas/>").appendTo($container);
   $canvas.css({'margin-top':-10});
   $canvas.height(320);
   $canvas.width(400);

   var ctx = $canvas[0].getContext("2d");
   var years = [];
   var colors = ['rgba(255,99,132,0.2)','rgba(54,162,235,0.2)','rgba(255,206,86,0.2)'];
   var borderColors = ['rgba(255,99,132,1)','rgba(54,162,235,1)','rgba(255,206,86,1)'];
   var labels = [],
   datasets = [],
   monthsWithData = {};
   data.forEach(function(x) {
    //if(x.CRIME_REGION==1900) return;
    if(x.CRIME_REGION == 19) {
        if ($.inArray(x.CRIME_YEAR, years) == -1) years.push(x.CRIME_YEAR);
        if (!monthsWithData[x.CRIME_MONTH]) monthsWithData[x.CRIME_MONTH] = {};
        if (!monthsWithData[x.CRIME_MONTH][x.CRIME_YEAR]) monthsWithData[x.CRIME_MONTH][x.CRIME_YEAR] = 0;
        monthsWithData[x.CRIME_MONTH][x.CRIME_YEAR] += +x.IND_VALUE
    }
  }) ;
   var labels = Object.keys(monthsWithData);
   labels.sort(function(a,b) {
    return a-b;
  })
   years.sort(function(a,b) {
    return +a-+b;
  })
   years.forEach(function (year,i) {
     dataset = {};
     dataset.label = year;
     dataset.backgroundColor = colors[i];
     dataset.borderColor = borderColors[i];
     dataset.borderWidth = 1;
     dataset.data = [];
     labels.forEach(function(monthNumber) {       
      dataset.data.push(monthsWithData[monthNumber][year]);
    })

     datasets.push(dataset);
   })

   labels = labels.map(function(x) {
    return (''+x).length==1?'0'+x:x
  })
   Chart.defaults.global.defaultFontColor = "#cfd2da";



   var grouppedBarChart = new Chart(ctx, {
    type: 'bar',

    data: {
      labels: labels,
      datasets: datasets
    },
    options: {
      hover: {
        animationDuration: 0
      },
      tooltips:{
        backgroundColor:'#000',
        bodyFontColor:'#cfd2da',
        callbacks: {
         title: function() {}
       }
     },
        // responsive: true,       
        // maintainAspectRatio: false,
        scales:{
          xAxes:[{
            gridLines: { display:false },
            ticks: { autoSkip:true, fontSize: 11,minRotation: 45,maxRotation: 45,fontColor:'#ccc'} 
          }],
          yAxes: [{
            gridLines: {
             color: '#46484f',
             lineWidth: 1
           }
         }]

       },
       title: {
        display: true
      },
      legend:{
        position:'bottom',
        labels:{
          padding:5
        }
      },
      animation: {
        duration: 1000
      },

    }
  });
 });
}