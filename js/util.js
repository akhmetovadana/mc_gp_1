
var Util = {
	Monthes: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
	MaxFromArray: function(arr) {
		return Math.max.apply(Math, arr);
	},
	MinFromArray: function(arr) {
		return Math.min.apply(Math, arr);
	},
	ObjectValues: function(obj) {
		var arr = [];

		for (var key in obj) {
			if (!obj.hasOwnProperty(key))
				continue;

			arr.push(obj[key]);
		}

		return arr;
	},
	ConvertArrayToObject: function(arr, keySelector, valueSelector) {
		var result = {};

		for (var i = 0; i < arr.length; i++)
		{
			var item = arr[i];
			result[keySelector(item)] = valueSelector(item);
		}

		return result;
	},
	GroupArrayBy: function(arr, keySelector, valueSelector) {
		valueSelector = valueSelector || function(x) { return x; };
		var result = {};

		for (var i = 0; i < arr.length; i++)
		{
			var item = arr[i];
			var key = keySelector(item);
			var value = valueSelector(item);

			result[key] = result[key] || [];
			result[key].push(value);
		}

		return result;
	},
	toDDMMYYYY: function(dateOrString) {
		if (!dateOrString.getTime) { // duck typing of Date object
			dateOrString = new Date(dateOrString);
		}

		var today = dateOrString;
		var dd = today.getDate();
		var mm = today.getMonth()+1;

		var yyyy = today.getFullYear();
		if(dd<10){
			dd='0'+dd;
		}
		if(mm<10){
			mm='0'+mm;
		}

		return dd+'.'+mm+'.'+yyyy;
	},
	fromDDMMYYYY: function(string) {
		var split = string.split(".");
		return new Date(split[2], split[1] - 1, split[0]);
	},
	fromDDMM: function(string) {
		var split = string.split(".");
		return new Date(2017, split[1] - 1, split[0]);
	},
	dateToYYYYMMDD: function(date) {
		var d = new Date(date),
		month = '' + (d.getMonth() + 1),
		day = '' + d.getDate(),
		year = d.getFullYear();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		return [year, month, day].join('-');
	},
	isLastDayOfMonth: function(dt) {
		var cloneDate = new Date(dt.getTime());
		var month = cloneDate.getMonth();
		cloneDate.setDate(cloneDate.getDate() + 1);
		return month !== cloneDate.getMonth();
	},
	normalizeRegionName: function(reg_id) {
		var res = '';
		switch	(reg_id.toString()) {
			case "1911":
			res = "Акмолинской области";
			break;
			case "1915":
			res = "Актюбинской области";
			break;
			case "1919":
			res = "Алмаатинской области";
			break;
			case "1923":
			res = "Атырауской области";
			break;
			case "1927":
			res = "Западно-Казахстанской области"
			break;
			case "1931":
			res = "Жамбылской области";
			break;
			case "1935":
			res = "Карагандинской области";
			break;
			case "1939":
			res = "Костанайской области";
			break;
			case "1943":
			res = "Кызылординской области";
			break;
			case "1947":
			res = "Мангыстауской области";
			break;
			case "1951":
			res = "Южно-Казахстанской области";
			break;
			case "1955":
			res = "Павлодарской области";
			break;
			case "1959":
			res = "Северо-Казахстанской области";
			break;
			case "1963":
			res = "Восточно-Казахстанской области";
			break;
			case "1971":
			res = "городе Астана";
			break;
			case "1975":
			res = "городе Алматы";
			break;
			case "1900":
			res = "Казахстане";
			break;
			default:
			res = '';
		}
		return res;
	},
	normalizeAnalyzeName: function(a_id) {
		var res = '';
		switch	(a_id.toString()) {
			case "4":
			res = "числа зарегистрированных преступлений по месяцам";
			break;
			case "6":
			res = "аномалий динамики зарегистрированных преступлений по месяцам";
			break;
			case "5":
			res = "числа зарегистрированных преступлений по дням";
			break;
			case "10":
			res = "количества подозрительных переквалификации по дням";
			break;
			case "11":
			res = "количества подозрительных прекращений и отмен прекращений по дням";
			break;
			case "77":
			res = "количества подозрительных прерываний и возобновлений по дням";
			break;
			case "12":
			res = "несвоевременного ввода информационных учетных документов Е2";
			break;
			case "13":
			res = "просрочки заполняемости дел по ходатайству (более трех суток)";
			break;
			case "88":
			res = "заполняемости атрибутов карточки Е1";
			break;
			case '8':
			res = "структуры преступлений на сегодня (за тек. год)";
			break;
			case '9':
			res = "структуры преступлений на конец прошлого месяца (за тек. год)";
			break;
			default:
			res = '';
		}
		return res;
	},
	regionColor: {
		"1900": { color: "#FFFFFF" },
		"1911": { color: "#CD5C5C" },
		"1915": { color: "#DC143C" },
		"1919": { color: "#DB7093" },
		"1923": { color: "#FF7F50" },
		"1927": { color: "#FFD700" },
		"1931": { color: "#EE82EE" },
		"1935": { color: "#BA55D3" },
		"1939": { color: "#8A2BE2" },
		"1943": { color: "#32CD32" },
		"1947": { color: "#008000" },
		"1951": { color: "#008080" },
		"1955": { color: "#4682B4" },
		"1959": { color: "#1E90FF" },
		"1963": { color: "#7B68EE" },
		"1971": { color: "#0000FF" },
		"1975": { color: "#4B0082" }
	},
	chartColors: {
		red: 'rgb(255, 99, 132)',
		orange: 'rgb(255, 159, 64)',
		yellow: 'rgb(255, 205, 86)',
		green: 'rgb(75, 192, 192)',
		blue: 'rgb(54, 162, 235)',
		purple: 'rgb(153, 102, 255)',
		grey: 'rgb(201, 203, 207)'
	},
	colorsMapping: {
		"": {
			color: "#C7C7C7",
			color_light: "#EEEEEE",
			gaugeIndicatorValue: 0,
			gaugeIndicatorText: "N/A"
		},
		"BLUE": {
			color: "rgb(127, 193, 253)",
			color_light: "rgba(127, 193, 253, 0.5)",
			gaugeIndicatorValue: 15,
			gaugeIndicatorText: "Низкий"
		},
		"LITE_RED": {
			color: "rgb(252, 203, 200)",
			color_light: "rgba(252, 203, 200, 0.5)",
			gaugeIndicatorValue: 50,
			gaugeIndicatorText: "Средний"
		},
		"RED": {
			color: "rgb(226, 106, 106)",
			color_light: "rgba(247, 69, 59, 0.5)",
			gaugeIndicatorValue: 85,
			gaugeIndicatorText: "Высокий"
		},
		"YELLOW": {
			color: "rgb(255, 255, 0)",
			color_light: "rgba(255, 255, 0, 0.5)",
			gaugeIndicatorValue: 50,
			gaugeIndicatorText: "N/A"
		},
		"GREEN_YELLOW": {
			color: "rgb(148, 156, 0)",
			color_light: "rgba(148, 156, 0, 0.5)",
			gaugeIndicatorValue: 50,
			gaugeIndicatorText: "N/A"
		}
	}

}
