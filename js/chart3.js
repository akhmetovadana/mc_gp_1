var indId = -1;
var crimeRegion = -1;
var regions = {};

function drawChart3(ind_id,crime_region,isRaion) {

  if(isRaion){
    Promise.all([API.getMTCRegions(),API.getChart3DataRaion(ind_id,crime_region)])
    .then(function (data) {
      var raionsWithData = {};
      data[0].forEach(function (x) {
        regions[x.CRIME_REGION] = x;
      });
      $('#chartsContainer').empty()
      data[1].forEach(function(x) {
        if(!raionsWithData[x.CRIME_DIST]) raionsWithData[x.CRIME_DIST]=[];
        raionsWithData[x.CRIME_DIST].push(x);
      })
      for(var raionCode in raionsWithData){
        draw(raionsWithData[raionCode],raionCode);
      }
    })
  }else{
    API.getChart3Data(ind_id,crime_region).then(function (data) {
      indId = ind_id;
      crimeRegion = crime_region;
      draw(data);
      setInterval(function () {
        if(indId != ind_id || crimeRegion != crime_region) return;
        draw(data)
      },1500)
    })
  }
}

function draw(data,raionCode) {
        console.log('data',data,raionCode)

  var height = 220,
  width = 350,
  margin=30,
  rawData = [];
// создание объекта svg
if(raionCode){
  // если максимальное значение равно 0 то выходим
  if(d3.max(data, function(d) { return +d.IND_VALUE} )==0) return;
  var container = d3.select("#chartsContainer").append('div').attr('class','col-xs-4');
  console.log(raionCode)
  var text = container.append('h4').classed('text-center',true).style('color','#fff').text(regions[raionCode].CRIME_REGION_DESC);
  var svg = container.append("svg");
}
else{
  $('#chart3').empty();
  var svg = d3.select("#chart3");
}

svg
.attr("class", "axis")
.attr("width", width)
.attr("height", height);
// длина оси X= ширина контейнера svg - отступ слева и справа
var xAxisLength = width - 2 * margin;

// длина оси Y = высота контейнера svg - отступ сверху и снизу
var yAxisLength = height - 2 * margin;

data.forEach(function(d) {
  d.IND_TIME = new Date(d.IND_TIME)
  d.IND_VALUE = +d.IND_VALUE
})

data.sort(function(a,b) {
  return a.IND_TIME - b.IND_TIME
})
if(data.length>0){
  var mindate = data[0].IND_TIME,
  maxdate = data[data.length-1].IND_TIME,
  maxY = d3.max(data, function(d) { return +d.IND_VALUE} ),
  minY = d3.min(data, function(d) { return +d.IND_VALUE});
  if(maxY == minY){
     minY = 0;
     if(maxY==0) maxY=1
  }
}else{
  var mindate = new Date((new Date).getFullYear(), (new Date).getMonth()-1, 1),
  maxdate = new Date((new Date).getFullYear(), (new Date).getMonth(), 1),
  maxY = 1,
  minY = 0;
}

// функция интерполяции значений на ось Х
var scaleX = d3.time.scale()
.domain([mindate,maxdate])
.range([0, xAxisLength]);

// функция интерполяции значений на ось Y
var scaleY = d3.scale.linear()
.domain([maxY,minY])
.range([0, yAxisLength]);

// масштабирование реальных данных в данные для нашей координатной системы
for(i=0; i<data.length; i++)
  rawData[i] = {IND_TIME: scaleX(data[i].IND_TIME)+margin, IND_VALUE: scaleY(data[i].IND_VALUE) + margin};
// создаем ось X
var xAxis = d3.svg.axis()
.scale(scaleX)
.orient("bottom")
.tickFormat(d3.time.format("%d.%m"))
.ticks(function(start, end) {
  var arr = [];
  for(var i=start;i<=end;){
    arr.push(i.getTime())
    i.setDate(i.getDate()+5)
  }
  arr.forEach(function (x,i) {
    arr[i] = new Date(x);
  })
  arr.push(end)
  return arr;
})

// создаем ось Y
var yAxis = d3.svg.axis()
.scale(scaleY)
.orient("left")

 // отрисовка оси Х
 svg.append("g")
 .attr("class", "x-axis")
     .attr("transform",  // сдвиг оси вниз и вправо
       "translate(" + margin + "," + (height - margin) + ")")
     .call(xAxis)
     .selectAll("text")
     .style({"text-anchor":"end",'font-size': '11px','fill':'#ccc'})
     .attr("transform", "rotate(-45)");;

 // отрисовка оси Y
 svg.append("g")
 .attr("class", "y-axis")
    .attr("transform", // сдвиг оси вниз и вправо на margin
      "translate(" + margin + "," + margin + ")")
    .call(yAxis)
     .selectAll("text")
     .style({"text-anchor":"end",'font-size': '11px','fill':'#ccc'});

// рисуем горизонтальные линии
d3.selectAll("g.y-axis g.tick")
.append("line")
.classed("grid-line", true)
.attr("x1", 0)
.attr("y1", 0)
.attr("x2", xAxisLength)
.attr("y2", 0);

// функция, создающая по массиву точек линии
var line = d3.svg.line()
.interpolate("basis")
.x(function(d){return d.IND_TIME;})
.y(function(d){return d.IND_VALUE;});
// добавляем путь
var path = svg.append("g").append("path")
.attr("d", line(rawData))
.style("stroke", "steelblue")
.style("stroke-width", 4);

var totalLength = path.node().getTotalLength();

path
.attr("stroke-dasharray", totalLength + " " + totalLength)
.attr("stroke-dashoffset", totalLength)
.transition()
.duration(500)
.ease("linear")
.attr("stroke-dashoffset", 0);
}
