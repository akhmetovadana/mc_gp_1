var regions = {
    "1911":"akmola",
    "1915":"aktubinsk",
    "1919":"almatinskaya",
    "1923":"atyrau",
    "1927":"ZKO",
    "1931":"Zhambyl",
    "1935":"karaganda",
    "1939":"kostanay",
    "1943":"Kyzylorda",
    "1947":"mangystau",
    "1951":"UKO",
    "1955":"pavlodar",
    "1959":"SKO",
    "1963":"VKO",
    "1971":"astana",
    "1975":"almaty"
};
function getIndValuesForDate(ind_id, crime_region) {
  API.getChart3Data(ind_id,crime_region).then(function (data1) {
    data1.sort(function(a,b) {
      return new Date(b.IND_TIME)-new Date(a.IND_TIME);
  });
    data1[0].IND_TIME = new Date(data1[0].IND_TIME);
    var date = data1[0].IND_TIME.getDate()+"."+(data1[0].IND_TIME.getMonth()+1)+"."+data1[0].IND_TIME.getFullYear();
    $("#last_day_crimes").html(data1[0].IND_VALUE+' на '+date);
});
}

function getIndName(ind_id,reg_id){
    API.getIndicators().then(function(data){

    var a = "";
    var b = data.sort(function(a, b){
        return a.id - b.id;
    });
    b.forEach(function(x){
        var row = x.IND_ID+" "+ x.IND_DESC;
        a+=row+"\n";
    });
    // $('#indicatorNames').text(a).css({'color': 'white'});
      API.getChart1Data(ind_id,reg_id).then(function(infos){
        for (var obj in data) {
          for(var i in infos){
            if (data[obj].IND_ID===ind_id){
              if(infos[i].CRIME_YEAR==="2017" && infos[i].CRIME_MONTH==="11" && infos[i].CRIME_REGION==="19"){
                $(".indicator-name").html(data[obj].IND_DESC.toUpperCase()+ " ПО РЕСПУБЛИКЕ: "+infos[i].IND_VALUE);
              }
            }
          }
        }
      });
    });
}

function getIndValuesForMap(ind_id){
    API.getMtcYear(ind_id).then(function(data){
        data.forEach(function(x){
            for(var key in regions) {
                if (key==x.CRIME_REGION){
                  // if (x.IND_VALUE < 10){$('#'+regions[key]).text(x.IND_VALUE);}
                  // else if (x.IND_VALUE < 100){$('#'+regions[key]).text(x.IND_VALUE).css({'font-size': '20px'});}
                  // else if (x.IND_VALUE < 500){$('#'+regions[key]).text(x.IND_VALUE).css({'font-size': '25px'});}
                  // else if (x.IND_VALUE > 500){$('#'+regions[key]).text(x.IND_VALUE).css({'font-size': '30px'});}
                  let fs = 14+Math.sqrt(x.IND_VALUE);
                  if (fs>44){fs = fs/4;}
                  $('#'+regions[key]).text(x.IND_VALUE).css({'font-size': fs+'px'});
                }
            }
        });
    });
}

function getIndValueP(ind_id, region_id){
    API.getMtcYear(ind_id).then(function(data){
        data.forEach(function(x){
            if(x.CRIME_REGION==region_id){
                $('#ind_value_p').text((+x.IND_VALUE_P*100).toFixed(0)+"%");
            }
        });
    });
}

function getRepublicCrimesNumber(ind_id){
    var dates = {};
    var year;
    var years = [];
    Promise.all([API.getPeriods(),API.getChart1Data(ind_id)]).then(function(data){
        var lastDate = new Date(data[0][data[0].length-1].crime_time),
        lastMonth = lastDate.getMonth()+1,
        lastYear = lastDate.getFullYear();
        data[1].forEach(function(x){
            if(x.CRIME_YEAR==""+lastYear && x.CRIME_MONTH==""+lastMonth  && x.CRIME_REGION=='19'){
                var isPrirostPositive = +x.PRIROST>0,
                prirostText = (isPrirostPositive?"+":"")+((+x.PRIROST)*100).toFixed(0);
                $('#prirost_value').text(prirostText+"%").css('color',isPrirostPositive?'red':'blue')
                $('.indicator-name').append(x.IND_VALUE);
                return;
            }
        });
    });
    // API.getMtcYear(ind_id).then(function(data){
    //     data.forEach(function(x){
    //         if(x.CRIME_REGION=='19'){
    //             $('#crimes_per_ind').text(x.IND_VALUE+" ПО РЕСПУБЛИКЕ");
    //             return;
    //         }
    //     });
    // });
}


function mapBadges(){
  var margin_top = -70;
  var margin_left = -25;
  var cur_margin_top = 0;
  var cur_margin_left= 0;
  var actual_margin_top= 0;
  var actual_margin_left= 0;
  for(var key in regions){
    cur_margin_top = $('#'+regions[key]).css("margin-top").replace(/[^-\d\.]/g, '');;
    cur_margin_left = $('#'+regions[key]).css("margin-left").replace(/[^-\d\.]/g, '');;
    actual_margin_top = parseInt(cur_margin_top)+margin_top;
    actual_margin_left = parseInt(cur_margin_left)+margin_left;
    $('#'+regions[key]).css({"margin-top": actual_margin_top+"px", "margin-left": actual_margin_left+"px"})
}

}

function showRegionName(reg_id,ind_id){
    API.getRegions().then(function(data){
        API.getMtcYear(ind_id).then(function(infos){
          for (var obj in data) {
              for(var i in infos){
                if (data[obj].CRIME_REGION==reg_id){
                  if (infos[i].CRIME_REGION==reg_id){
                      $(".region-name").html(data[obj].CRIME_ID_DESC.toUpperCase()+": "+infos[i].IND_VALUE);
                  }
                }
              }
          }
        });
    });
}