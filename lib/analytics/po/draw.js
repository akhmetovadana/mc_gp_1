var paintPieChart = function(region, reg_id){
  var arr = []
  API.getRegionsData(Global.ind_id)
  .then(function(res){
   var oblasti = res;
   var data = [];
   j = 0;
   for (var i = 0; i < oblasti.length; i++) {
    if(oblasti[i].CRIME_REGION == reg_id){
      data[j] = oblasti[i];
      var score = oblasti[i].IND_MEAN;
      j++;
    };
  }

  for (i in data){
    if (data[i].CRIME_REGION == reg_id){
      arr.push(data[i].IND_VALUE_PERCENT);
    }
  }
  var maxim = Math.max.apply(null, arr);
  console.log("MAXIM"+maxim);

  API.getPopulationData()
  .then(function(res){
    var regions = res;


    API.getRegionTitles()
    .then(function(res){
      var width, height;
        width = 200, height = 200

      //  else {
      //   width = 70, height = 70
      // }
      radius = Math.min(width, height) / 2,
      innerRadius = 0.3 * radius;
      var regTitle = res;

      var pie = d3.layout.pie()
      .sort(null)

      .value(function(d) { return d.CRIME_DIST; });

      for (var i = 0; i < data.length; i++) {
        for (var j = 0; j < regions.length; j++) {
         if(data[i].CRIME_DIST == regions[j].CODE){
          data[i].POPULATION = regions[j].POPULATION;
        }
      }
    }

    for (var i = 0; i < data.length; i++) {
      for (var j = 0; j < regTitle.length; j++) {
        if(data[i].CRIME_DIST == regTitle[j].CRIME_REGION){
          data[i].REGION_TITLE = regTitle[j].CRIME_REGION_DESC;
        }
      }
    }

    var tip = d3.tip()
    .attr('class', 'd3-tip')
    .offset([0, 0])
    .html(function(d) {
      return d.data.REGION_TITLE + "<br>Кол-во населения: <span style='color:orangered'>"
      + d.data.POPULATION + "</span><br>"
       + "Количество: " + "<span style='color:orangered'>" + (d.data.IND_VALUE_PERCENT*1).toFixed(0) + "</span><br>" ;
    });
    var arc = d3.svg.arc()
    .innerRadius(innerRadius)
    .outerRadius(function (d) {
      arr.push(Number(d.data.IND_VALUE_PERCENT));
      return (radius - innerRadius) * (d.data.IND_VALUE_PERCENT/maxim) + innerRadius;
    });

    var outlineArc = d3.svg.arc()
    .innerRadius(innerRadius)
    .outerRadius(function(d){
      return (radius - innerRadius) * (d.data.IND_VALUE_PERCENT)/100 + radius;
    });

    $(region).empty();
    var svg = d3.select(region).append("svg")
    .attr("width", width)
    .attr("height", height)
    .append("g")
    .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

    svg.call(tip);
    var outerPath = svg.selectAll(".outlineArc")
    .data(pie(data))
    .enter().append("path")
    .attr("fill", "#1A232E")
    .attr("stroke", "#1A232E")
    .attr("class", "outlineArc")
    .attr("d", outlineArc)
    .on('mouseover', tip.show)
    .on('mouseout', tip.hide);

    var path = svg.selectAll(".solidArc")
    .data(pie(data))
    .enter().append("path")
    .attr("fill", function(d){return d.data.CRIME_DIST_COLOR;})
    .attr("class", "solidArc")
    .attr("stroke", "gray")
    .attr("d", arc)
    .on('mouseover', tip.show)
    .on('mouseout', tip.hide);

    svg.append("svg:text")
    .attr("class", "aster-score")
    .attr("dy", ".35em")
    .attr("text-anchor", "middle") // text-align: right
    .attr("fill", "white")
    .text(Math.round(score));
  });
  });
});
}


