
define([
    "esri/graphic",
    "esri/symbols/SimpleFillSymbol",
    "esri/Color",
    "esri/renderers/ClassBreaksRenderer",
    "esri/symbols/TextSymbol",
    "esri/symbols/Font",
    "esri/symbols/SimpleLineSymbol",
    "dijit/TooltipDialog",
    "analytics/po/ArcGis",
    "dojo/query",
    "analytics/po/draw"
    ], function (Graphic, SimpleFillSymbol, Color, ClassBreaksRenderer, TextSymbol, Font, SimpleLineSymbol, TooltipDialog, ArcGis, query, draw) {
        var map, dialog;
        var ias3url = "http://projects.kurmangaliyev.kz/GenProsecutorOffice-3-Beta/";
        $(document).ajaxError(function(ev, xhr, settings) {
            console.error("Не удалось загрузить " + settings.url + ". Ответ сервера: " + xhr.status + "(" + xhr.responseText + ")");
        });

    // Строим карту
    var buildMapPromise = ArcGis.buildKazakhstanMap({
        container: "map",
        map: "kpsisu",
        regionClick: function(e) {
           var obj = e.graphic;
           if (obj) {
            var content = "";
            if (obj.attributes) {
                Global.region_id = obj.attributes.CODE;
                var reg = {

                    CRIME_REGION: obj.attributes.CODE,
                    IND_ID: Global.ind_id,
                };
                console.log("on click region");
                MatrixChart.changeMatrixCell(reg);
            }
        }
    }
});

    // Загружаем области
    var loadArcGisOblastsPromise = ArcGis.getOblasts().then(function(data) {
        var arcGisOblasts = {};

        data.forEach(function(x) {
            arcGisOblasts[x.attributes.CODE] = x;
            x.attributes.CLUSTER = "CLUSTER_15";
            x.attributes.NAME = x.attributes.S1;
        });
        return arcGisOblasts;
    });



    Global.paintMap = function() {
      console.log('paint map');
      Promise.all([buildMapPromise,loadArcGisOblastsPromise, API.getMatrix(),API.getChart1Data(1)])
      .then(function(results) {
        map = results[0];
        var oblasts = results[1];
        var data = results[2];
        var crimes = results[3];


        for (var regionCode in oblasts) {

          oblasts[regionCode].attributes.COLOR = Util.colorsMapping["BLUE"].color;

      }

      var ready_data = [];



      function setSelectedIndicatorId(ind){ 
        for (var obj in data) {
            if (data[obj].IND_ID===ind){
                ready_data.push(data[obj]);
            }
        }
        return ready_data;
    }
    var neonColors = {'BLUE':'#36a2eb','LITE_RED':'#8c4259','RED': '#842837'}

    setSelectedRegionCode(Global.region_id);
    setSelectedIndicatorId(Global.ind_id);
    ready_data.forEach(function(x) {
      oblasts[x.CRIME_REGION].attributes.COLOR = neonColors[x.VALUE_COLOR];
  });

    map.bind(oblasts);
        // map.regionClick = function(e) {
        //   console.log(e);
				// 	setSelectedRegionCode(e.graphic.attributes.CODE);
				// 	// renderAll();
				// }

                function setSelectedRegionCode(regionCode) {
                    var selectedRegionsOblast = oblasts[regionCode];
                    if (selectedRegionsOblast.geometry) {
                        var highlightGraphicOblast = new Graphic(selectedRegionsOblast.geometry, new SimpleLineSymbol(
                            SimpleLineSymbol.STYLE_SOLID,
                            new Color([255, 255, 255]),
                            3
                            ));

                        map.canvasHighlights.clear();
                        map.canvasHighlights.add(highlightGraphicOblast);
                        map.canvasHighlights.redraw();
                    }
                }
            });

  }
})
